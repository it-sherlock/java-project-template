/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp168t05xk88zzbv7ro.mysql.rds.aliyuncs.com:3306
 Source Schema         : db2019

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 20/06/2022 17:37:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'IO',
  `serial` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES (1, 'zhangsan');
INSERT INTO `payment` VALUES (12, '李四3');
INSERT INTO `payment` VALUES (13, '李四3');
INSERT INTO `payment` VALUES (14, '李四5');
INSERT INTO `payment` VALUES (15, NULL);
INSERT INTO `payment` VALUES (16, 'zhangsan');

SET FOREIGN_KEY_CHECKS = 1;
