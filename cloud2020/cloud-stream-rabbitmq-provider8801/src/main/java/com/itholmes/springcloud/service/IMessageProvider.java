package com.itholmes.springcloud.service;

/**
 * @author: xuyanbo
 * @description: TODO
 * @date: 2022/8/3 22:51
 */
public interface IMessageProvider {
    public String send();
}
