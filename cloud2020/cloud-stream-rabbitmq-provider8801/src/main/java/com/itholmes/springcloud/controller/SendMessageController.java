package com.itholmes.springcloud.controller;

import com.itholmes.springcloud.service.IMessageProvider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: xuyanbo
 * @description: TODO
 * @date: 2022/8/3 23:02
 */
@RestController
public class SendMessageController {

    //直接注入进来就可以
    @Resource
    private IMessageProvider messageProvider;

    @GetMapping(value = "/sendMessage")
    public String sendMessage(){
        return messageProvider.send();
    }

}
