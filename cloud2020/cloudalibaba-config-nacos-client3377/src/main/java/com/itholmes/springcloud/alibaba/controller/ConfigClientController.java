package com.itholmes.springcloud.alibaba.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: xuyanbo
 * @description: TODO
 * @date: 2022/8/6 11:14
 */
@RestController
@RefreshScope // 通过Spring Cloud原生注解,@RefreshScope实现配置的自动更新。
public class ConfigClientController {

    @Value("${config.info}")
    private String configInfo;

    @GetMapping(value = "/config/info")
    public String getConfigInfo(){
        return configInfo;
    }

}
