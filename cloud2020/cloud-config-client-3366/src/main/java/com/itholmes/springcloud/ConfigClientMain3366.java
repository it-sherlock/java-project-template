package com.itholmes.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @projectName: cloud2020
 * @package: PACKAGE_NAME
 * @className: com.itholmes.springcloud.ConfigClientMain3366
 * @author: xuyanbo
 * @description: TODO
 * @date: 2022/7/28 22:38
 * @version: 1.0
 */
@EnableEurekaClient
@SpringBootApplication
public class ConfigClientMain3366 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigClientMain3366.class,args);
    }
}
