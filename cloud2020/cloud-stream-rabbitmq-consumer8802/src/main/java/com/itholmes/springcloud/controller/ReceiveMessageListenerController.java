package com.itholmes.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @description:
 *      注解：@EnableBinding(Sink.class)的作用
 *            注解@EnableBinding指信道channel和exchange绑定在一起
 *            Sink.class代表接受
 * @date: 2022/8/4 13:26
 *
 */
@Component
@EnableBinding(Sink.class)
public class ReceiveMessageListenerController {

    @Value("${server.port}")
    private String serverPort;

    //监听Sink的INPUT
    @StreamListener(Sink.INPUT)
    public void input(Message<String> message){
        System.out.println("消费者1号，-----》 接受到的消息： " + message.getPayload() + "\t port:" + serverPort);
    }


}
