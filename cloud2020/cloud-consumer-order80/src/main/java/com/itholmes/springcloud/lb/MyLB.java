package com.itholmes.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class MyLB implements LoadBalancer{

    /**
     *  AtomicInteger类是系统底层保护的int类型，通过提供执行方法的控制进行值的原子操作。
     *  AtomicInteger它不能当作Integer来使用
     *
     */
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    public final int getAndIncrement(){
        int current;
        int next;
        do {
            current = this.atomicInteger.get();
            //2147483647是integer的最大值。
            next = current >= 2147483647 ? 0 : current + 1;

        /**
         * java.util.concurrent.atomic.AtomicInteger.compareAndSet()是Java中的一种内置方法，
         * 如果当前值等于参数给定的期望值，则将值设置为参数中的传递值。该函数返回一个布尔值，该布尔值使我们了解更新是否完成。
         */
        }while (!this.atomicInteger.compareAndSet(current,next));
        System.out.println("*****第几次访问次数，next:"+next);
        return next;
    }

    @Override
    public ServiceInstance instances(List<ServiceInstance> serviceInstances) {
        //轮询算法
        int index = getAndIncrement() % serviceInstances.size();
        return serviceInstances.get(index);
    }
}
