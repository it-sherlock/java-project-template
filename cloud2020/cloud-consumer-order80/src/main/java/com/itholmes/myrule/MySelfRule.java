package com.itholmes.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//还是要注意这个类不能设置在@ComponentScan所扫描当前包下以及子包下！！
@Configuration
public class MySelfRule {

    //注入IRule接口对应的一些实现类
    @Bean
    public IRule myRule(){
        //随机规则
        return new RandomRule();
    }


}
