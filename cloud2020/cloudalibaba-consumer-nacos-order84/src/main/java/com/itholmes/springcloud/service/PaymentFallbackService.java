package com.itholmes.springcloud.service;

import com.itholmes.springcloud.entities.CommonResult;
import com.itholmes.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentService{
    /**
     * 上面请求没有成功就会触发服务降级，走对应实现类的方法。
     */
    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(444,"服务降级返回，--PaymentFallbackService",new Payment(id,"errorSerial"));
    }
}
