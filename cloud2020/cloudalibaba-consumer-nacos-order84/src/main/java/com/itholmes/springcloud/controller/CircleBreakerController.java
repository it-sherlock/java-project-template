package com.itholmes.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.itholmes.springcloud.entities.CommonResult;
import com.itholmes.springcloud.entities.Payment;
import com.itholmes.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @description: TODO
 * @date: 2022/8/12 20:38
 */
@RestController
@Slf4j
public class CircleBreakerController {
//
//    public static final String SERVICE_URL = "http://nacos-payment-provider";
//
//    @Resource
//    private RestTemplate restTemplate;
//
//    @RequestMapping("/consumer/fallback/{id}")
//    @SentinelResource(
//            value = "fallback",
//            fallback = "handlerFallback",
//            blockHandler = "blockHandler"
//    )
//    public CommonResult<Payment> fallback(@PathVariable Long id){
//        CommonResult<Payment> result = restTemplate.getForObject(SERVICE_URL + "/paymentSQL/" + id, CommonResult.class, id);
//        if (id == 4){
//            throw new IllegalArgumentException("IllegalArgument非法参数异常");
//        }else if (result.getData() == null){
//            throw new NullPointerException("NullPointException空指针异常");
//        }
//        return result;
//    }
//
//    public CommonResult handlerFallback(@PathVariable Long id,Throwable e){
//        Payment payment = new Payment(id, "null");
//        return new CommonResult(444,"兜底异常handlerFallback，异常信息" + e.getMessage(),payment);
//    }
//
//    public CommonResult blockHandler(@PathVariable Long id,Throwable e){
//        Payment payment = new Payment(id, "null");
//        return new CommonResult(444,"触发限流规则blockHandler，异常信息" + e.getMessage(),payment);
//    }


    @Resource
    private PaymentService paymentService;

    @GetMapping(value = "/consumer/paymentSQL/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id ){
        return paymentService.paymentSQL(id);
    }

}
