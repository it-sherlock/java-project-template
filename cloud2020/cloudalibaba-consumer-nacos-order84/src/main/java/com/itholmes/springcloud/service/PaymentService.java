package com.itholmes.springcloud.service;

import com.itholmes.springcloud.entities.CommonResult;
import com.itholmes.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *  value指定微服务名称。
 *
 *  只要访问的接口出现问题，就会走服务降级，走fallback指定对应的接口实现类方法。
 *      fallback会走对应实现该接口的实现类对应重写的该方法。
 */
@FeignClient(value = "nacos-payment-provider",fallback = PaymentFallbackService.class)
public interface PaymentService {
    @GetMapping(value = "/paymentSQL/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id);
}
