package com.itholmes.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // @LoadBalanced负载均衡注解很重要，不要忘记配置，，没有它启动不起来项目
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
