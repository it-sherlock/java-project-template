package com.itholmes.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: TODO
 * @date: 2022/8/12 20:36
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class MainOrder84 {
    public static void main(String[] args) {
        SpringApplication.run(MainOrder84.class,args);
    }
}
