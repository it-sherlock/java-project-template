package com.itholmes.springcloud.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.itholmes.springcloud.entities.CommonResult;
/**
 * @description: TODO
 * @date: 2022/8/11 21:22
 */
public class CustomerBlockHandler {
    public static CommonResult handlerException(BlockException exception){
        return new CommonResult(200,"按客户自定定义--global");
    }
    public static CommonResult handlerException2(BlockException exception){
        return new CommonResult(200,"按客户自定定义--global2");
    }
}
