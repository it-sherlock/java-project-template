package com.itholmes.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GateWayConfig {

    //使用注入bean的方式来修改网关的路由
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();

        // 这里映射百度的地址 , 目录为https://news.baidu.com/guonei
        //这样访问http://localhost:9527/guonei 就会 访问到 https://news.baidu.com/guonei的信息
        routes.route("path_route_itholmes",
                r -> r.path("/guonei")
                .uri("https://news.baidu.com/guonei"));

        return routes.build();
    }
}
