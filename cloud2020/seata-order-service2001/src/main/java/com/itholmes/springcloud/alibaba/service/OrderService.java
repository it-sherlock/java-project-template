package com.itholmes.springcloud.alibaba.service;

import com.itholmes.springcloud.alibaba.domain.Order;


public interface OrderService {
    void create(Order order);
}
