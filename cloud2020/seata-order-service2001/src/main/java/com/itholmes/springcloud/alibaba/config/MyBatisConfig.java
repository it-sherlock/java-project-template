package com.itholmes.springcloud.alibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.itholmes.springcloud.alibaba.dao"})
public class MyBatisConfig {
}
