import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:spring.xml"})
public class SSMTest {
    @Resource
    EmpMapper empMapper;
    @Test
    public void ssmTest1(){
        List<Emp> emps = empMapper.allEmp();
        System.out.println();
    }
}
