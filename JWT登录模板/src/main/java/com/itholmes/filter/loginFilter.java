package com.itholmes.filter;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.itholmes.pojo.User;
import com.itholmes.service.LoginService;
import com.itholmes.utils.JedisPools;
import com.itholmes.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
@Repository
public class loginFilter implements Filter {

    @Autowired
    LoginService loginService;


    public void init(FilterConfig filterConfig) throws ServletException {
        /**
         * 由于web应用启动的顺序是：listener>filter>servlet  所以才filter初始化的时候手动添加SpringBean支持。这个是重点的重点！
         * 目的就是为了防止上面的Service自动装配能够装配进来。
         */
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, filterConfig.getServletContext());
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        //如果访问/或者不是html目录下的文件直接放行
        if (
                req.getRequestURI().contains("/js/") ||
                req.getRequestURI().equals("/Login.html") ||
                req.getRequestURI().equals("/secure/login")
        ) {
            chain.doFilter(req, resp);
            return;
        }

        //方式二：使用JWT技术
        String token = req.getHeader("Authorization");
        if (token != null && !token.equals("")) {
            DecodedJWT verify = JwtUtil.verify(token);
            User user = JwtUtil.parse(verify);
            if (JedisPools.getLoginJedis(user.getUser_name(), user.getUser_password())) {
                chain.doFilter(req, resp);
                return;
            }
        }


        System.out.println(req.getContextPath());
        resp.sendRedirect(req.getContextPath() + "/Login.html");
        return;
    }

    public void destroy() {

    }
}
