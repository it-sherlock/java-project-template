package com.itholmes.service;

import com.itholmes.pojo.User;

public interface LoginService {
    //查询单个用户登录
    User selectOneUser(String username);
}
