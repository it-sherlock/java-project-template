package com.itholmes.service;

import com.itholmes.dao.LoginMapper;
import com.itholmes.pojo.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class LoginServiceImpl implements LoginService{

    @Resource
    LoginMapper loginMapper;

    //查询单个用户信息
    public User selectOneUser(String username) {
        return loginMapper.selectOneUser(username);
    }

}
