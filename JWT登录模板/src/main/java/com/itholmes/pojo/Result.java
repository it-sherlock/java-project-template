package com.itholmes.pojo;

public class Result {
    public int Code;
    public Object Data;
    public String Msg;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public Result() {
    }

    public Result(int code, Object data, String msg) {
        this.Code = code;
        this.Data = data;
        this.Msg = msg;
    }

    public Result(int code, String msg) {
        this.Code = code;
        this.Msg = msg;
    }

}
