package com.itholmes.pojo;

import lombok.Data;

@Data
public class User {
    String user_id;
    String user_name;
    String user_password;
}
