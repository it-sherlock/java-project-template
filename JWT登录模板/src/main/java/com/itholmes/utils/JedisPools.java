package com.itholmes.utils;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ResourceBundle;

public class JedisPools {

    private static Jedis jedis;

    private static Integer expireTime = 5 * 60 * 60;//默认5分钟 3600 * 5 = 18000

    static {

        ResourceBundle db = ResourceBundle.getBundle("db");
        expireTime = Integer.parseInt(db.getString("expireTime"));

        //redis连接池配置
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxTotal(100);
        poolConfig.setMaxIdle(50);
        poolConfig.setMinIdle(20);
        JedisPool jedisPool = new JedisPool(poolConfig, "127.0.0.1", 6379);
        jedis = jedisPool.getResource();
    }

    //登录成功加入
    public static boolean setLoginJedis(String key, String value) {
        String setex = jedis.setex(key, expireTime, value);
        if (setex.toUpperCase().equals("OK")) {
            return true;
        }
        return false;
    }

    //查询redis
    public static boolean getLoginJedis(String key, String value) {
        String value2 = jedis.get(key);
        if (value2 != null) {
            if (value2.equals(value)) {
                return true;
            }
        }
        return false;
    }

    //请求redis
    public static boolean removeLoginJedis(String key){
        Long del = jedis.del(key);
        if (del>0){
            return true;
        }
        return false;
    }

}
