package com.itholmes.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itholmes.pojo.User;

import java.io.IOException;
import java.util.Calendar;
import java.util.ResourceBundle;

public class JwtUtil {

    //jwt的salt盐
    private static final String key = "www.itholmes.top";

    private static Integer expireTime = 5 * 60 * 60;//默认5分钟 3600 * 5 = 18000

    static {
        ResourceBundle db = ResourceBundle.getBundle("db");
        expireTime = Integer.parseInt(db.getString("expireTime"));
    }

    //生成token
    public static String generateToken(User user) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String user_string = mapper.writeValueAsString(user);

        //通过calendar设置过期时间
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND,expireTime);

        JWTCreator.Builder builder = JWT.create()
                .withClaim("userInfo", user_string)
                .withExpiresAt(calendar.getTime());

        String token = builder.sign(Algorithm.HMAC256(key));

        return token;
    }

    //校验token
    public static DecodedJWT verify(String tokenToVerity){
        DecodedJWT verify = null;
        try{
            verify = JWT
                    .require(Algorithm.HMAC256(key))
                    .build()
                    .verify(tokenToVerity);
        }catch(TokenExpiredException e) {
            e.printStackTrace();
            System.out.println("该token已经过期。");
        }catch (SignatureVerificationException e) {
            e.printStackTrace();
            System.out.println("签名不一致");
        }catch (AlgorithmMismatchException e) {
            e.printStackTrace();
            System.out.println("签名算法不匹配");
        }catch (InvalidClaimException e) {
            e.printStackTrace();
            System.out.println("payload不可用");
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("其他错误校验失败");
        }
        return verify;
    }

    //将DecodedJWT中的信息解析出来进行比对
    public static User parse(DecodedJWT decodedJWT) throws IOException {
        Claim claim = decodedJWT.getClaim("userInfo");
        if (claim!=null){
            String userString = claim.asString();
            ObjectMapper mapper = new ObjectMapper();
            User user = mapper.readValue(userString, User.class);
            return user;
        }
        return null;
    }


}
