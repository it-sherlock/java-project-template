package com.itholmes.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itholmes.pojo.Result;
import com.itholmes.pojo.User;
import com.itholmes.service.LoginService;
import com.itholmes.utils.JedisPools;
import com.itholmes.utils.JwtUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/secure")
public class LoginController {

    @Resource
    LoginService loginService;

    //登录
    @RequestMapping("/login")
    public Result secureLogin(
            @RequestBody(required = false) String s,
            HttpServletResponse response
    ) {

        ObjectMapper mapper = new ObjectMapper();

        String username = "";
        String password = "";
        User user = null;
        try {
            user = mapper.readValue(s, User.class);
            username = user.getUser_name();
            password = user.getUser_password();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (username == null || username.equals("")) {
            return new Result(0, "用户名为空");
        }

        if (password == null || password.equals("")) {
            return new Result(0, "密码为空");
        }

        String password_fromDB = "";
        try {
            password_fromDB = loginService.selectOneUser(username).getUser_password();
        } catch (NullPointerException e) {
            return new Result(0, "用户不存在");
        }

        if (password_fromDB.equals(password)) {

            //方式一：使用JWT来是实现
            String token = "";
            try {
                token = JwtUtil.generateToken(user);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            //登录成功，加入redis
            JedisPools.setLoginJedis(username, password);

            return new Result(1, token,"登录成功");
        } else {
            return new Result(0, "密码错误");
        }

    }

    //退出
    @RequestMapping("/quit")
    public Result quitLogin(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        boolean b = false;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {

                //使用JWT技术
                String[] split = cookie.getValue().split("&");
                if (split != null && split.length != 0) {
                    if (split[0] != null && !split[0].equals("") && split[1] != null && !split[1].equals("")) {
                        b = JedisPools.removeLoginJedis(split[0]);
                    }
                }

                if (!b){
                    return new Result(0, "退出失败");
                }

                cookie.setMaxAge(0);
                return new Result(1, "成功退出");
            }
        }

        return new Result(0, "用户未登录");
    }
}
