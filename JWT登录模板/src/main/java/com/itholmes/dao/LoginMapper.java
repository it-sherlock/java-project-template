package com.itholmes.dao;

import com.itholmes.pojo.User;

public interface LoginMapper {
    //查询单个用户登录
    User selectOneUser(String username);
}
